# Beanie Deploy

![Beanie Deploy Demo](README_files/beanie-demo.gif)

## Description


Beanie Deploy is a powerful tool that allows you to install native Fedora Linux quickly and seamlessly from your Windows machine. Beanie Deploy streamlines the installation process and does not require a spare USB stick.


## Why Beanie Deploy?

### Use this App if:

- **Save Time**: If you've just purchased a new Windows machine and want a super quick transition to Linux, Beanie Deploy is the perfect solution for you.

### Do not use this App if:

- **Trying Fedora Linux**: This App makes significant changes to the system. While you have the option install alongside Windows, the best way to try Fedora Linux is by booting the Live Environment from an external USB storage.

- **Unfamiliar with the Process / No Spare USB Stick**: Things can go wrong, potentially bricking your system temporarily. Therefore, it's recommended that you have some knowledge of manual OS installation and possess a spare USB stick.



## Acknowledgments

- Thanks to the [Fedora Project](https://fedoraproject.org/) for providing a robust Linux distribution.


- **Azure Theme for Tkinter**: Thanks to [Benedek Dévényi](https://github.com/rdbende) for creating the [Azure-ttk-theme](https://github.com/rdbende/Azure-ttk-theme) used in this project.

