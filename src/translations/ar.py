# Arabic translations
btn_next = "التالي"
btn_yes = "نعم"
btn_no = "لا"
btn_back = "العودة"
btn_quit = "إنهاء"
btn_cancel = "إلغاء"
btn_continue = "استمر"
btn_abort = "إحباط"
btn_confirm = "تأكيد"
btn_install = "تثبيت"
btn_restart_now = "إعادة التشغيل الآن"
btn_restart_later = "إعادة التشغيل لاحقا"
btn_retry = "إعادة المحاولة"

check_available_downloads = "التحقق من التنزيلات المتاحة"
check_existing_download_files = "التحقق من وجود ملفات من التثبيتات السابقة"
check_running = "التحقق من توافق النظام. يرجى الانتظار..."
check_ram = "التحقق من سعة الذاكرة العشوائية المتاحة"
check_uefi = "التحقق من الامتثال لـ UEFI"
check_space = "التحقق من المساحة المتاحة"
check_resizable = "التحقق من إمكانية تغيير حجم محرك النظام"

error_title = "%s لا يمكن أن يعمل على نظامك"
error_list = "تم اكتشاف القضايا التالية:"
error_arch_0 = "هندسة المعالج لهذا الجهاز غير متوافقة."
error_arch_9 = "تعذر التحقق من هندسة المعالج."
error_uefi_0 = "نظامك لا يدعم (أو لا يستخدم) الإقلاع UEFI."
error_uefi_9 = "تعذر التحقق من دعم الإقلاع UEFI."
error_totalram_0 = "نظامك لا يملك سعة ذاكرة عشوائية كافية."
error_totalram_9 = "فشل في التحقق من سعة الذاكرة العشوائية المتاحة."
error_space_0 = "لا يوجد مساحة كافية على محرك النظام الخاص بك. حرر مساحة وحاول مرة أخرى"
error_space_9 = "فشل في التحقق من المساحة المتاحة على محرك النظام الخاص بك."
error_resizable_0 = "لا يوجد مساحة كافية لتقليص محرك النظام."
error_resizable_9 = "فشل في التحقق من إمكانية تغيير حجم محرك النظام."

info_about_selection = "معلومات حول الاختيار:"
desktop_question = "حدد توزيعتك المفضلة."
desktop_hints = {'KDE Plasma': "KDE غني بالميزات، قابل للتخصيص بشكل كبير، ويحتوي على تخطيط مشابه لـ Windows بشكل افتراضي",
                 'GNOME': "سطح المكتب GNOME بسيط وثابت جدا، مُحسّن للوحة اللمس والشاشات التي تدعم اللمس"}
lang = "اللغة"
locale = "الإعدادات المحلية"
recommended = "موصى به"
adv = "متقدم"
net_install = "تثبيت الشبكة"
warn_space = "لا يوجد مساحة كافية"
warn_not_available = "غير متوفر"
total_download = "إجمالي التنزيل: %sGB"
init_download = "التنزيل الأولي: %sGB"

install_running = "التثبيت جارٍ..."
install_auto = "التثبيت السريع والموجه"
install_help = "ساعدني في القرار"
title_autoinst2 = "اختر لغتك وإعداداتك المحلية"
title_autoinst3 = "اختر منطقتك الزمنية وتخطيط لوحة المفاتيح الخاصة بك"
title_autoinst4 = "أنشئ حساب المستخدم المحلي الخاص بك"

additional_setup_now = "قم بإعداد الإعدادات المحلية والمنطقة الزمنية وتخطيط لوحة المفاتيح الآن"

selected_dist = "التوزيعة المحددة"
desktop_environment = "سطح المكتب"
choose_distro = "اختر توزيعة"
encrypted_root = "تشفير نظام التشغيل الجديد"
entry_encrypt_passphrase = "تعيين عبارة مرور التشفير"

encrypt_reminder_txt = "سيُطلب منك إدخال عبارة مرور التشفير قبل بدء التثبيت"
encryption_tpm_unlock = "الفتح التلقائي باستخدام TPM2"

entry_username = "اسم المستخدم"
entry_fullname = "الاسم الكامل (اختياري)"
password_reminder_txt = "سيكون بإمكانك تعيين كلمة مرور لحساب المستخدم الخاص بك بعد التثبيت"

list_keymaps = "تخطيط لوحة المفاتيح"
list_timezones = "المنطقة الزمنية"

immutable_system = "صورة النظام ثابتة"

something_else = "شيء آخر"

windows_question = "حسنًا! كيف يجب تثبيت %s؟"
windows_options = {'dualboot': 'تثبيت بجانب Windows',
                   'replace_win': 'تثبيت واستبدال Windows',
                   'custom': 'تثبيت وتقسيم مخصص'}
dualboot_size_txt = "المساحة المحجوزة لـ %s:"
warn_backup_files_txt = "احفظ ملفاتك! سيتم حذف كل شيء داخل %s بشكل دائم"
verify_question = "هذا ما سيحدث. انقر على %s لبدء التثبيت" % btn_install
verify_text = {
    'no_autoinst': "%s سيتم تنزيله وسيتم التشغيل عند الإعادة التالية لبدء التثبيت المخصص.",
    'autoinst_dualboot': "%s سيتم تنزيله وتثبيته بجانب Windows.",
    'autoinst_replace_win': "%s سيتم تنزيله وتثبيته لاستبدال Windows.",
    'autoinst_keep_data': "لن يتأثر البيانات الحالية.",
    'autoinst_rm_all': "سيتم مسح كل شيء على هذا الجهاز.",
    'autoinst_wifi': "سيتم إضافة ملفات تعريف Wi-Fi الحالية في %s."
}
add_import_wifi = "تصدير شبكات Wi-Fi الخاصة بي إلى %s"
enable_rpm_fusion = "تمكين المستودعات الإضافية لدعم البرامج الموسعة وترميز الوسائط المتعددة"
add_auto_restart = "إعادة التشغيل تلقائيا"
add_torrent = "استخدم التورنت عندما يكون ذلك ممكنا"
more_options = "المزيد من الخيارات"

distro_hint = {'Fedora Workstation': 'الإصدار الرئيسي من Fedora مع بيئة سطح المكتب GNOME',
               'Fedora KDE Plasma': 'Fedora مع بيئة سطح المكتب KDE Plasma'}

job_starting_download = "بدء التنزيل..."
downloads_number = "الملف %s من %s"
job_dl_install_media = "تنزيل وسائط التثبيت..."
dl_timeleft = "الوقت المتبقي"
dl_speed = "السرعة"

keymap_tz_option = "المنطقة الزمنية الافتراضية وتخطيط لوحة المفاتيح لـ %s"
keymap_tz_ip_description = "(منطقة IP الخاصة بك)"
keymap_tz_selected_description = "(الإعدادات المحلية التي اخترتها)"
keymap_tz_custom = "اختر المنطقة الزمنية وتخطيط لوحة المفاتيح"

job_checksum = "التحقق من سلامة الملف المُنزّل"
job_checksum_failed = "فشل التحقق من السلامة"
job_checksum_failed_txt = "فشل في التحقق من سلامة الملف المُنزّل، هل تريد المتابعة على أي حال؟"
job_checksum_mismatch = "عدم تطابق الخواص"
job_checksum_mismatch_txt = "الملف المُنزّل لديه خاصية SHA256 غير متوقعة ولا يمكن الوثوق به.\n" \
                            "\nخاصية الملف:\n%s" \
                            "\nالخاصية المتوقعة:\n%s\n" \
                            "\nعادةً ما يعني هذا أن الملف لم يتم تنزيله بشكل صحيح." \
                            "\nهل تريد المحاولة مرة أخرى للتنزيل؟"

job_creating_tmp_part = "إنشاء قسم تمهيدي مؤقت لوسائط التثبيت..."
job_copying_to_tmp_part = "نسخ الملفات المطلوبة لوسائط التثبيت إلى القسم التمهيدي المؤقت..."
job_adding_tmp_boot_entry = "إضافة مدخل التمهيد..."

cleanup_question = "تنظيف الملفات المُنزّلة؟"
cleanup_question_txt = "هذه الملفات لن تكون مفيدة بعد الآن إلا إذا كنت تخطط لإعادة استخدام التطبيق لاحقًا.\nهل تريد حذفها؟"
finished_title = "مطلوب إعادة تشغيل"
finished_text = "مطلوب إعادة تشغيل لمتابعة التثبيت، " \
                   "انقر على '%s' لإعادة التشغيل الآن أو على '%s' لإعادة التشغيل يدويًا في وقت لاحق" % (btn_restart_now, btn_restart_later)
finished_text_restarting_now = "إعادة التشغيل التلقائي في %s ثوانٍ"

lang_question = "الآن دعنا نبدأ، حدد لغتك المفضلة."
